//
//  SecondViewController.swift
//  QuickShare
//
//  Created by Doan Huy Binh on 9/30/16.
//  Copyright © 2016 Doan Huy Binh. All rights reserved.
//

import UIKit
import Social


class SecondViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    @IBOutlet weak var imageView: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func cameraTapped(_ sender: AnyObject) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        if imagePickerController.sourceType == .camera
        {
            imagePickerController.sourceType = .camera
        }
        else
        {
            imagePickerController.sourceType = .photoLibrary
        }
        self.present(imagePickerController, animated: true, completion: nil)
        
        
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickerImage = info[UIImagePickerControllerOriginalImage] as? UIImage
        {
            self.imageView.image  = pickerImage
            self.dismiss(animated: true, completion: nil)
        }
        
    }
    
    @IBAction func shareTapped(_ sender: AnyObject) {
        
        if imageView.image != nil
        {
            let twitter = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            twitter?.add(imageView.image)
            self.present(twitter!, animated: true, completion: nil)
        }
        else
        {
            print("No Image Selected to share On Twitter")
        }

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

